-- Matteo Cusini's research code: Simulator settings
--(MaxIter, Tol, CFL)
TIMESTEPS
	3000

REPORTS
	100

--Minimum and Maximum allowed time-step (in days)
MINMAXDT
	1e-3 30

OUTPUT
	VTK

--Fully Implicit Method (max iteration, residual tolerances (each equation), solution tolerances (each equation), CFL)	
FIM
	10
	1e-2,1e-2
	1e-4,1e-4
	100
	
LINEARSOLVER
	direct
	
--Algebraic Dynamic Multilevel method settings
ADM
	1
LEVELS
	2
COARSENING_RATIOS
	3
	3
	3
COARSENING_CRITERION
	dfdx
VARIABLE
	T
TOLERANCE
	5
	
--Constant, Homogeneous, MS
PRESSURE_INTERPOLATOR
	MS
	DECOUPLED
HYPERBOLIC_INTERPOLATOR
	Constant
ROCK_TEMPERATURE_INTERPOLATOR
	Constant
	
-- END OF DECK 
