-- Matteo Cusini's research code: Simulator settings
--(MaxIter, Tol, CFL)
TIMESTEPS
	5000

REPORTS
	100

--Minimum and Maximum allowed time-step (in days)
MINMAXDT
	1e-3 30

OUTPUT
	VTK

--Fully Implicit Method (max iteration, residual tolerances (each equation), solution tolerances (each equation), CFL)	
FIM
	15
	1e-3,1e-2
	1e-3,1e-3
	100
	
LINEARSOLVER
	direct
	
--Algebraic Dynamic Multilevel method settings
ADM
	1
LEVELS
	2
COARSENING_RATIOS
	5
	5
	1
COARSENING_CRITERION
	dfdx
VARIABLE
	T
TOLERANCE
	40
	
--Constant, Homogeneous, MS
PRESSURE_INTERPOLATOR
	MS
	COUPLED
HYPERBOLIC_INTERPOLATOR
	Constant
	
-- END OF DECK 
