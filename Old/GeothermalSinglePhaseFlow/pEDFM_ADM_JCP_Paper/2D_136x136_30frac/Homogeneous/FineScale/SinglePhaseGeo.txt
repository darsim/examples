-- DARSim2 INPUT file
-- All keywords are in capital letters

--Name
TITLE
	SinglePhaseGeo

--Size of the reservoir in m 
DIMENS
 	100.00
	100.00
	001.00
	
--Total time (in days and hours and minutes and seconds)
TOTALTIME
	1000 DAYS 0 HOURS 0 MINUTES 0 SECONDS
	
--Permeability
	--To specify unit (m2, D, mD), add "permeability_unit" keyword in capital letters. Default is m2)
	--To specify scale (Linear or Logarithmic), add "permeability_scale" keyword in capital letters. Default is Linear. If the data is in log scale, use "logarithmic" keyword in capital letters.
	--In case of using heterogenous input file, you can choose to permanently reduce the heterogenous contrast by using "contrast_reduction" in capital letters.
	PERMEABILITY_UNIT
		m2
			
	PERMEABILITY_SCALE
		Linear

		PERMX
			1e-14
			
		PERMY
			1e-14
			
		PERMZ
			1e-14
			
POR
	0.2 
	
TEMPERATURE (K)
	400

GRAVITY
	OFF

--Fluid/Rock Properties
DENSITY (kg/m^3)
	--Phase 1 
	1000
	--Rock
	2750

VISCOSITY (Pa sec)
	--Phase 1
	1e-3

COMPRESSIBILITY (1/Pa)
	--Phase 1
	0e-10
	--Rock
	0e-11
	
CONDUCTIVITY (W/m/K)
	--Phase 1
	0.591
	--Rock
	4
	
SPECIFIC HEAT (J/Kg/K)
	--Phase 1
	4200
	--Rock
	790

RELPERM (Linear,Quadratic,Corey,Foam)
	Linear
	--srw (wetting residual saturation)
	0.0
	--srnw (nonwetting residual saturation)
	0.0
	--Wetting kre [-]
	1
	--Nonwetting kre [-]
	1
	--Wetting n [-]
	2
	--Nonwetting n [-]
	2
	
CAPILLARITY (Linear,JLeverett,BrooksCorey,Table)

FLUID MODEL (Immiscible,BlackOil,Compositional,Geothermal_1T, Geothermal_2T)
	Geothermal_2T
	--Number of phases
	1
	--Number of components
	1
--Extra keyword "averaged_temperature" for Geothermal_2T whether to consider two separate temperatures (Tf, Tr) or to average them.
	AVERAGED_TEMPERATURE
	
INIT ( Pressure(pa), Saturation phase 1 to n )
    1.5e7 1 1
	
--grid Nx//Ny//Nz
SPECGRID	
	136
	136
	001

--Wells ( [i;j;k](start), [i;j;k](end), constraint (pressure - rate), value (Pa - pv/day), type(PI/radius), value(m^3/day/pa - m), temperature (K) )
INJ
	1
	1
	1
	1
	1
	NZ
	pressure
	2e7
	PI
	1000
	--temperature_inj
	300

INJ
	1
	1
	NY
	NY
	1
	NZ
	pressure
	2e7
	PI
	1000
	--temperature_inj
	300
	
PROD
	NX
	NX
	1
	1
	1
	NZ
	pressure
	1e7
	PI
	1000

PROD
	NX
	NX
	NY
	NY
	1
	NZ
	pressure
	1e7
	PI
	1000

--Fractures keyword "fractured"
	FRACTURED

-- END OF DECK 
