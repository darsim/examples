--DARSim2FraGen: INPUT file

--Type of Generator(either "EDFM" or "pEDFM")
TYPE
	pEDFM

--Dimension of Domain (either 2D or 3D)
DOMAIN
    2D

--Size of the reservoir in m (Length, Width, Thickness)
DIMENS
 	100.00
	100.00
	001.00
	
--GRID Nx//Ny//Nz
SPECGRID
	125
	125
	001
	
--ADM Status for Reservoir: Activation(0,1), Level(1,2,etc.), Coarsening Ratio along x,y,z
ADM_RESERVOIR
	0,2,[1,1,1]
	
--Comparison Accuracy (The level of accuracy in comparing two values or in zero comparison)
CompareAccuracy
	1e-09

--Fractures Properties
    // Activation (0,1)                              % Whether fracture is used (1) or not (0)
	// Central Coordination [x;y;z]                  % The coordinate of the central point of fracture plate
	// Length [m]                                    % The length of fracture plate
	// Width [m]                                     % The width of fracture plate
	// Rotation Angle Along Z [deg]                  % Angle of rotation along the z-axis while the fracture is first completely horizontal (on xy plane)
	// Rotation Angle Along Central Length [deg]     % Angle of rotation along the central length axis that is connecting the center of both lengths of fracture together
	// Rotation Angle Along Central Width [deg]      % Angle of rotation along the central width axis that is connecting the center of both widths of fracture together
	// Grid Resolution Ratio [-]                     % The ratio between the fracture and reservoir grid resolution. If it is less than 1, the fracture gridding is coarser than matrix and if it is more than 1, fracture gridding is finer than matrix
	// Grid Number Along Length [-]                  % The manual grid numbers of the fracture plate along its length (ignoring Grid Resolution Ratio)
	// Grid Number Along Width [-]                   % The manual grid numbers of the fracture plate along its width (ignoring Grid Resolution Ratio)
	// Aperture [m]                                  % The aperture of fracture plate
	// Porosity [-]                                  % The porosity of fracture plate
	// Permeability [m^2]                            % Permeability of fracture plate
	// ADM Configuration                             % Activation(0,1) , Level(1,2,etc.) , Coarsening Ratio along Length , Coarsening Ratio along Width
	// Corner Points [ x1,x2,x3;y1,y2,y3;z1;z2;z3 ]  % The coordinates of three corners defining the fracture plate (can be used as an alternative way of positioning the fracture)
FRAC_START
	1 , [1*LX/2 ; 1*LY/2 ; 1*LZ/2] , 1*LX/2 , 1*LZ/1 , 000 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1e-06 , [0,1,1,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 01
	1 , [1*LX/2 ; 1*LY/2 ; 1*LZ/2] , 1*LX/2 , 1*LZ/1 , 090 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1e-06 , [0,1,1,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 02
	0 , [1*LX/2 ; 1*LY/2 ; 1*LZ/2] , 1*LX/2 , 1*LZ/1 , 045 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1e-06 , [0,1,1,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 03
	0 , [1*LX/2 ; 1*LY/2 ; 1*LZ/2] , 1*LX/2 , 1*LZ/1 , 135 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1e-06 , [0,1,1,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 04
FRAC_END
--END OF DECK