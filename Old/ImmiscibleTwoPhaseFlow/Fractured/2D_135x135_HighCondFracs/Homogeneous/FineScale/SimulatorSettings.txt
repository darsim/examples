-- Matteo Cusini's research code: Simulator settings
--(MaxIter, Tol, CFL)
TIMESTEPS
	500
REPORTS
	50

OUTPUT
	VTK
	
FIM	
	15
	1e-6
	100
LINEARSOLVER
	direct
--Implicit transport Solver (only if Sequential)
--(fluxfunct, tol, maxiter)
IMPSAT
	2
	1e-6
	40
	
--Algebraic Dynamic multilevel method settings
ADM
	0
LEVELS
	2
COARSENING_RATIOS
	3
	3
	1
COARSENING_CRITERION
	dfdx
VARIABLE
    S_1
TOLERANCE
	0.3
--Constant, Homogeneous, MS
PRESSURE_INTERPOLATOR
	MS
	COUPLED
HYPERBOLIC_INTERPOLATOR
	Constant

-- END OF DECK
