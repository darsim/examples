--DARSim2FraGen: INPUT file

--Type of Generator(either "EDFM" or "pEDFM")
TYPE
	EDFM

--Dimension of Domain (either 2D or 3D)
DOMAIN
    2D

--Size of the reservoir in m (Length, Width, Thickness)
DIMENS
 	100.0
	100.0
	001.0
	
--GRID Nx//Ny//Nz
SPECGRID
	135
	135
	001
	
--Comparison Accuracy (The level of accuracy in comparing two values or in zero comparison)
CompareAccuracy
    1e-10

--Fractures Properties
    // Activation (0,1)                              % Whether fracture is used (1) or not (0)
	// Central Coordination [x;y;z]                  % The coordinate of the central point of fracture plate
	// Length [m]                                    % The length of fracture plate
	// Width [m]                                     % The width of fracture plate
	// Rotation Angle Along Z [deg]                  % Angle of rotation along the z-axis while the fracture is first completely horizontal (on xy plane)
	// Rotation Angle Along Central Length [deg]     % Angle of rotation along the central length axis that is connecting the center of both lengths of fracture together
	// Rotation Angle Along Central Width [deg]      % Angle of rotation along the central width axis that is connecting the center of both widths of fracture together
	// Grid Resolution Ratio [-]                     % The ratio between the fracture and reservoir grid resolution. If it is less than 1, the fracture gridding is coarser than matrix and if it is more than 1, fracture gridding is finer than matrix
	// Grid Number Along Length [-]                  % The manual grid numbers of the fracture plate along its length (ignoring Grid Resolution Ratio)
	// Grid Number Along Width [-]                   % The manual grid numbers of the fracture plate along its width (ignoring Grid Resolution Ratio)
	// Aperture [m]                                  % The aperture of fracture plate
	// Porosity [-]                                  % The porosity of fracture plate
	// Permeability [m^2]                            % Permeability of fracture plate
	// ADM Configuration                             % Activation(0,1) , Level(1,2,etc.) , Coarsening Ratio along Length , Coarsening Ratio along Width
	// Cornor Points [ x1,x2,x3;y1,y2,y3;z1;z2;z3 ]  % The coordinates of three corners defining the fracture plate (can be used as an alternative way of positioning the fracture)
FRAC_START
	1 , [03.5*LX/14 ; 06.8*LY/14 ; 1*LZ/2] , 0.77*LX/01 , 1.00*LZ/01 , 075 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 01
	0 , [06.4*LX/14 ; 07.4*LY/14 ; 1*LZ/2] , 0.94*LX/01 , 1.00*LZ/01 , 040 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 02
	0 , [06.1*LX/14 ; 07.0*LY/14 ; 1*LZ/2] , 0.87*LX/01 , 1.00*LZ/01 , 110 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 03
	0 , [10.0*LX/14 ; 10.0*LY/14 ; 1*LZ/2] , 0.56*LX/01 , 1.00*LZ/01 , 155 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 04
	0 , [11.9*LX/14 ; 07.6*LY/14 ; 1*LZ/2] , 0.50*LX/01 , 1.00*LZ/01 , 080 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 05
    0 , [03.0*LX/14 ; 01.6*LY/14 ; 1*LZ/2] , 0.25*LX/01 , 1.00*LZ/01 , 150 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 06
	0 , [02.9*LX/14 ; 02.3*LY/14 ; 1*LZ/2] , 0.19*LX/01 , 1.00*LZ/01 , 070 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 07
	0 , [01.4*LX/14 ; 08.3*LY/14 ; 1*LZ/2] , 0.17*LX/01 , 1.00*LZ/01 , 120 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 08
	0 , [01.9*LX/14 ; 08.9*LY/14 ; 1*LZ/2] , 0.25*LX/01 , 1.00*LZ/01 , 055 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 09
	0 , [02.2*LX/14 ; 10.9*LY/14 ; 1*LZ/2] , 0.30*LX/01 , 1.00*LZ/01 , 125 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 10
	0 , [02.5*LX/14 ; 07.8*LY/14 ; 1*LZ/2] , 0.25*LX/01 , 1.00*LZ/01 , 088 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 11
	0 , [03.2*LX/14 ; 08.0*LY/14 ; 1*LZ/2] , 0.26*LX/01 , 1.00*LZ/01 , 130 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 12
	0 , [04.9*LX/14 ; 04.4*LY/14 ; 1*LZ/2] , 0.17*LX/01 , 1.00*LZ/01 , 020 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 13
	0 , [05.5*LX/14 ; 03.5*LY/14 ; 1*LZ/2] , 0.22*LX/01 , 1.00*LZ/01 , 040 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 14
	0 , [05.0*LX/14 ; 03.1*LY/14 ; 1*LZ/2] , 0.25*LX/01 , 1.00*LZ/01 , 095 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 15
	0 , [06.7*LX/14 ; 02.9*LY/14 ; 1*LZ/2] , 0.38*LX/01 , 1.00*LZ/01 , 025 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 16
	0 , [07.0*LX/14 ; 03.3*LY/14 ; 1*LZ/2] , 0.46*LX/01 , 1.00*LZ/01 , 120 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 17
	0 , [05.3*LX/14 ; 10.2*LY/14 ; 1*LZ/2] , 0.33*LX/01 , 1.00*LZ/01 , 080 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 18
	0 , [05.5*LX/14 ; 11.5*LY/14 ; 1*LZ/2] , 0.14*LX/01 , 1.00*LZ/01 , 150 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 19
	0 , [06.2*LX/14 ; 06.5*LY/14 ; 1*LZ/2] , 0.17*LX/01 , 1.00*LZ/01 , 085 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 20
	0 , [07.2*LX/14 ; 11.0*LY/14 ; 1*LZ/2] , 0.33*LX/01 , 1.00*LZ/01 , 040 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 21
	0 , [07.3*LX/14 ; 08.8*LY/14 ; 1*LZ/2] , 0.20*LX/01 , 1.00*LZ/01 , 115 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 22
	0 , [07.1*LX/14 ; 09.2*LY/14 ; 1*LZ/2] , 0.11*LX/01 , 1.00*LZ/01 , 040 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 23
	0 , [08.3*LX/14 ; 04.2*LY/14 ; 1*LZ/2] , 0.38*LX/01 , 1.00*LZ/01 , 065 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 24
	0 , [09.2*LX/14 ; 04.4*LY/14 ; 1*LZ/2] , 0.25*LX/01 , 1.00*LZ/01 , 145 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 25
	0 , [08.2*LX/14 ; 09.3*LY/14 ; 1*LZ/2] , 0.48*LX/01 , 1.00*LZ/01 , 100 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 26
	0 , [09.2*LX/14 ; 08.3*LY/14 ; 1*LZ/2] , 0.46*LX/01 , 1.00*LZ/01 , 097 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 27
	0 , [09.1*LX/14 ; 07.4*LY/14 ; 1*LZ/2] , 0.25*LX/01 , 1.00*LZ/01 , 040 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 28
	0 , [10.0*LX/14 ; 02.0*LY/14 ; 1*LZ/2] , 0.16*LX/01 , 1.00*LZ/01 , 135 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 29
	0 , [10.3*LX/14 ; 02.1*LY/14 ; 1*LZ/2] , 0.22*LX/01 , 1.00*LZ/01 , 045 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 30
	0 , [11.1*LX/14 ; 02.9*LY/14 ; 1*LZ/2] , 0.18*LX/01 , 1.00*LZ/01 , 110 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 31
	0 , [11.7*LX/14 ; 03.3*LY/14 ; 1*LZ/2] , 0.22*LX/01 , 1.00*LZ/01 , 165 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 32
	0 , [12.3*LX/14 ; 03.9*LY/14 ; 1*LZ/2] , 0.27*LX/01 , 1.00*LZ/01 , 085 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 33
	0 , [11.9*LX/14 ; 05.0*LY/14 ; 1*LZ/2] , 0.22*LX/01 , 1.00*LZ/01 , 145 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 34
	0 , [11.3*LX/14 ; 06.0*LY/14 ; 1*LZ/2] , 0.25*LX/01 , 1.00*LZ/01 , 050 , 090 , 000 , 1.0 , NaN , NaN , 5e-3 , 1.0 , 1.00e-8 , [1,2,3,1] , [ NaN,NaN,NaN ; NaN,NaN,NaN ; NaN,NaN,NaN ] , 35
FRAC_END
--END OF DECK