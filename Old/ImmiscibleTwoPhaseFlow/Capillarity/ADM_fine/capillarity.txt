-- Matteo Cusini's research code: INPUT file 

--Name
TITLE
	capillarity

--Size of the reservoir in m 
DIMENS
	500.0
	500.0
	1.0
--Total time in days
TOTALTIME
	1350

--Permeability
INCLUDE
	PERMX
        Perm_45deg_1.txt
INCLUDE
    PERMY 
        Perm_45deg_1.txt
    PERMZ
        1e-12
PERMEABILITY_UNIT
    mD
    
POR
	0.2

TEMPERATURE (K)
	450

GRAVITY
	OFF

--Fluid Properties
DENSITY
	-WATER
	1
	-OIL
	800
COMPRESSIBILITY (1/Pa)
	--Phase 1
	0
	--Phase 2
	0
VISCOSITY
	--WATER
	1e-4
	--OIL
	1e-3

RELPERM
	Quadratic
	--swr
	0
	--sor
	0
	--Wetting kre [-]
	1
	--Nonwetting kre [-]
	1
	--Wetting n [-]
	2
	--Nonwetting n [-]
	2

CAPILLARITY
	JLeverett
	1


FLUID MODEL (Immiscible,BlackOil,Compositional)
	Immiscible
	--Number of phases
	2
	--Number of components
	2

--GRID Nx//Ny//Nz 
SPECGRID	
	99
	99 
	1	
INIT
	1.5e7 0 1
--Wells (i, j, pressure, radius)
INJ1
	50
	50
	50
	50
	1
	1
	pressure
	1.5e7
	PI
	10000
PROD1
	99
	99   
	99
	99
	1
	1
	pressure
	1e7
	PI
	10000
PROD2
	1
	1   
	99
	99
	1
	1
	pressure
	1e7
	PI
	10000
PROD3
	99
	99   
	1
	1
	1
	1
	pressure
	1.2e7
	PI
	10000
PROD3
	1
	1   
	1
	1
	1
	1
	pressure
	1.2e7
	PI
	10000

-- END OF DECK 
