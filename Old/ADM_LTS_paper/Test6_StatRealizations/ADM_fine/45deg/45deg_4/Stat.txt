-- DARSim research code: INPUT file

--Name
TITLE
Case1_45deg_4

--Size of the reservoir in m 
DIMENS
        500.0
	500.0
	1.0
--Total time in days
TOTALTIME
	565

--Permeability
INCLUDE
	PERMX
45deg/Perm_45deg_4.txt
INCLUDE
    PERMY 
45deg/Perm_45deg_4.txt
    PERMZ
        1e-12

PERMEABILITY_UNIT
    mD
POR
	0.2

TEMPERATURE (K)
	450

GRAVITY
	OFF

--Fluid Properties
DENSITY
	-WATER
	1
	-OIL
	1
COMPRESSIBILITY (1/Pa)
	--Phase 1
	0
	--Phase 2
	0
VISCOSITY
	--WATER
	1e-5
	--OIL
	10e-5

RELPERM
	Quadratic
	--swr
	0
	--sor
	0
	--Wetting kre [-]
	1
	--Nonwetting kre [-]
	1
	--Wetting n [-]
	2
	--Nonwetting n [-]
	2

CAPILLARITY

FLUID MODEL (Immiscible,BlackOil,Compositional)
	Immiscible
	--Number of phases
	2
	--Number of components
	2

--GRID Nx//Ny//Nz 
SPECGRID	
	99
	99 
	1	
INIT
	1.5e6 0 1
--Wells (i, j, pressure, radius)
INJ1
        1
        1
	1
	1
	1
        1
	pressure
	1e7
	PI
	10000
PROD1
	99
	99   
	99
	99
        1
        1
	pressure
	0
	PI
	10000

-- END OF DECK 





