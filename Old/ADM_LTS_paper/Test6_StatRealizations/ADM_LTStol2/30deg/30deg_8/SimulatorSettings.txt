-- Matteo Cusini's research code: Simulator settings
--(MaxIter, Tol, CFL)
TIMESTEPS
	10000
MINMAXDT
    5 20
REPORTS
	0

OUTPUT
	VTK
	
SEQUENTIAL
	1
	1e-3
	80
LINEARSOLVER
	direct
--Implicit transport Solver (only if Sequential)
--(fluxfunct, tol, maxiter)
IMPSAT
	1
	1e-2
	60

--Multirate (Local time steps only if Sequential)
LTS
    1
-- Fixed, Adaptive
REF_CRITERION
    Adaptive
TIME_TOL
    5e-2
PLOT_INTERNAL_STEPS
    0

--Algebraic Dynamic multilevel method settings
ADM
	1
LEVELS
	2
COARSENING_RATIOS
	3
	3
	1
COARSENING_CRITERION
	dfdx2
VARIABLE
	S_1
TOLERANCE
	0.08
--Constant, Homogeneous, MS
PRESSURE_INTERPOLATOR
    MS
HYPERBOLIC_INTERPOLATOR
	Constant
-- END OF DECK
