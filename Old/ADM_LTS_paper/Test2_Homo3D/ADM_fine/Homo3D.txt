-- JCP ADM-LTS test case 2: Input file

--Name
TITLE
	Homo3D

--Size of the reservoir in m 
DIMENS
 	108.0
	108.0
	108.0
	
--Total time in days
TOTALTIME
	8750

--Permeability
	PERMX
		5e-15
	PERMY
		5e-15
	PERMZ
		5e-15

POR
	0.2
	
TEMPERATURE (K)
	450

GRAVITY
	OFF

--Fluid/Rock Properties
DENSITY (kg/m^3)
	--Phase 1 
	1
	--Phase 2
	1

VISCOSITY (Pa sec)
	--Phase 1
	1e-3
	--Phase 2
	10e-3

COMPRESSIBILITY (1/Pa)
	--Phase 1
	0
	--Phase 2
	0

RELPERM (Linear,Quadratic,Corey,Foam)
	Quadratic
	--srw (wetting residual saturation)
	0
	--srnw (nonwetting residual saturation)
	0
	--Wetting kre [-]
	1
	--Nonwetting kre [-]
	1
	--Wetting n [-]
	2
	--Nonwetting n [-]
	2
	
CAPILLARITY (Linear,JLeverett,BrooksCorey,Table)	

FLUID MODEL (Immiscible,BlackOil,Compositional)
	Immiscible
	--Number of phases
	2
	--Number of components
	2

INIT ( Pressure(pa), Saturation phase 1 to n )
	1.5e7 0 1
	
--GRID Nx//Ny//Nz 
SPECGRID	
	54
	54
	54

--Wells (i_start, i_end, j_start, j_end, k_start, k_end, constrain ("pressure" or "rate"), PI (number or "Dirichlet"), radius)
INJ1
	1
	1
	1
	1
	1
	1
	pressure
	10e7
	PI
	1000

PROD1
	NX
	NX
	NY
	NY
	NZ
	NZ
	pressure
	0
	PI
	1000

--Fractures
	FRACTURE:
	

-- END OF DECK 



