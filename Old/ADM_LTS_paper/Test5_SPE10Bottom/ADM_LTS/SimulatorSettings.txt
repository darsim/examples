-- Matteo Cusini's research code: Simulator settings
--(MaxIter, Tol, CFL)
TIMESTEPS
	10000
MINMAXDT
    1 10
REPORTS
	0

OUTPUT
	VTK
SEQUENTIAL 
	1
	1e-4
	40
LINEARSOLVER
	direct
--Implicit transport Solver (only if Sequential)
--(fluxfunct, tol, maxiter)
IMPSAT
	2
	1e-4
	80
--Multirate (Local time steps only if Sequential)
LTS
    1
-- Fixed, Adaptive
REF_CRITERION
    Fixed
TIME_TOL
    5e-3
PLOT_INTERNAL_STEPS
    0
--Algebraic Dynamic multilevel method settings
ADM
	1
LEVELS
	2 
COARSENING_RATIOS
	3
	3
	1
COARSENING_CRITERION
	dfdx2
VARIABLE
	S_1
TOLERANCE
	0.05
--Constant, Homogeneous, MS
PRESSURE_INTERPOLATOR
	MS
	COUPLED
HYPERBOLIC_INTERPOLATOR
	Constant
ADT_SEQ
    0
-- END OF DECK 
