-- Matteo Cusini's research code: INPUT file 

--Name
TITLE
	SPE10B

--Size of the reservoir in m 
DIMENS
 	2200
	600
	1
--Total time in days
TOTALTIME
	505

--Permeability
INCLUDE
	PERMX
        SPE10/K_spe10B.txt
INCLUDE
    PERMY 
        SPE10/K_spe10B.txt
    PERMZ 
        1e-12
PERMEABILITY_UNIT
    mD
POR
	0.2

TEMPERATURE (K)
	450

GRAVITY
	OFF

--Fluid Properties
DENSITY
	-WATER
	1
	-OIL
	1
COMPRESSIBILITY (1/Pa)
	--Phase 1
	0
	--Phase 2
	0
VISCOSITY
	--WATER
	1e-5
	--OIL
	10e-5

RELPERM
	Quadratic
	--swr
	0
	--sor
	0
	--Wetting kre [-]
	1
	--Nonwetting kre [-]
	1
	--Wetting n [-]
	2
	--Nonwetting n [-]
	2

CAPILLARITY

FLUID MODEL (Immiscible,BlackOil,Compositional)
	Immiscible
	--Number of phases
	2
	--Number of components
	2

--GRID Nx//Ny//Nz 
SPECGRID	
	216
	54
	1
INIT
	1.5e6 0 1
--Wells (i, j, pressure, radius)
INJ1
	1
	1
	54
	54
	1
	1
	pressure
	1e7
	PI
	10000
PROD1
	216
	216
	1
	1
	1
	1
	pressure
	0
	PI
	10000

-- END OF DECK 
