-- Simulator settings file for DARSim Simulator (https://gitlab.com/darsim)
-- All keywords are in capital letters
-- The name of this file must be "SimulatorSettings.txt" (case sensitive).


REPORTS
	100


MINMAXDT (days)
	1 30


OUTPUT
	VTK


FIM
	20
	1e-5,1e-5
	1e-4,1e-3


LINEARSOLVER
	direct