-- Main input file for DARSim Simulator (https://gitlab.com/darsim)
-- All keywords are in capital letters


TITLE
	SPE10T


DIMENSION
 	220
	60
	1
	
	
SPECGRID
	216
	54
	1
	
	
TOTAL_TIME
	1 DAYS 0 HOURS 0 MINUTES 0 SECONDS
	

PERMEABILITY_UNIT
	mD
	
PERMEABILITY_SCALE
	Linear
		
INCLUDE	
PERMEABILITY_X
	Perm_SPE10T.txt
	
INCLUDE
PERMEABILITY_Y
	Perm_SPE10T.txt
		
INCLUDE
PERMEABILITY_Z
	Perm_SPE10T.txt


POROSITY
	0.2 


FLUID_DENSITY [kg/m^3]
	1000
	800


FLUID_VISCOSITY [Pa.sec]
	1e-3
	3e-3


FLUID_COMPRESSIBILITY [1/Pa]
	0
	0

	
ROCK_COMPRESSIBILITY [1/Pa]
	0


RELPERM (Linear,Quadratic,Corey,Foam)
	Linear
	--srw (wetting residual saturation)
	0.0
	--srnw (nonwetting residual saturation)
	0.0
	--Wetting kre [-] (end-point relative permeability for the wetting phase)
	1
	--Nonwetting kre [-] (end-point relative permeability for the non-wetting phase)
	1
	--Saturation exponent for the wetting phase [-]
	1
	--Saturation exponent for the non-wetting phase [-]
	1
	

FLUID_MODEL (SinglePhase or Immiscible)
	SinglePhase
	--Number of phases
	1
	--Number of components
	1


INITIAL_PRESSURE [Pa]
	2e7
INITIAL_SATURATION_1 [-]
	1.0
	

WELL_START
	TYPE
		INJ
	COORDINATE
		IJK_LIST
		[ 1 , 1 , 1  ]
		[ 1 , 1 , NZ ]
	CONSTRAINT
		PRESSURE
		5e7
	FORMULA
		WI
		1000
WELL_END

WELL_START
	TYPE
		PROD
	COORDINATE
		IJK_LIST
		[ NX , NY , 1  ]
		[ NX , NY , NZ ]
	CONSTRAINT
		PRESSURE
		1e7
	FORMULA
		WI
		1000
WELL_END

-- END OF DECK