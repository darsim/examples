-- Simulator settings file for DARSim Simulator (https://gitlab.com/darsim)
-- All keywords are in capital letters
-- The name of this file must be "SimulatorSettings.txt" (case sensitive).

TIMESTEPS
	2500

REPORTS
	100

--Minimum and Maximum allowed time-step (in days)
MINMAXDT
	1e-3 30

OUTPUT
	VTK
	BINARY

-- The couping strategy (either fully-implicit method "FIM" or sequantial method "Sequential", in capital letters)
FIM
	30
	1e-4, 1e-4
	1e-3, 1e-3
	100
	
LINEARSOLVER
	direct
	
--Algebraic Dynamic Multilevel method settings
ADM
	1
LEVELS
	2
COARSENING_RATIOS
	5
	5
	1
COARSENING_CRITERION
	dfdx
VARIABLE
	T
TOLERANCE
	5
	
--Constant, Homogeneous, MS
PRESSURE_INTERPOLATOR
	MS
	COUPLED
	BASIS_FUNCTION_MAX_CONTRAST
		1e2
	pEDFM_MAX_CONTRAST
		1e2
		
HYPERBOLIC_INTERPOLATOR
	Constant
	
-- END OF DECK 
